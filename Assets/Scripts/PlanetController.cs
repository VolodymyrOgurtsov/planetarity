﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    private const float START_HEALTH = 20.0f;

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Transform planet;
    [SerializeField] Transform rocketStartLocation;
    [SerializeField] RocketController rocketPrefab;
    [SerializeField] GameObject heathBarObject;
    [SerializeField] GameObject cooldownBarObject;
    [SerializeField] Transform heathBar;
    [SerializeField] Transform cooldownBar;

    private bool isPlayer;
    private float radius;
    private float angleSpeed;
    private float angle;
    private RocketParameters rocketParameters;
    private float launchTimer;
    private bool isInited;
    private bool isActive;
    private Func<Vector3, Vector2> getGravitationForce;
    private Func<Vector3> getPlayerPosition;
    private float health;
    private Color32 playerColor = Color.green;
    private float launchCooldown;
    private bool isDead;
    private bool isPaused;
    private List<RocketController> rockets = new List<RocketController>();
    private Action<PlanetController> onDeath;

    public bool IsDead => isDead;

    public void Init(float radius, float angleSpeed, float angle, Color32 color, RocketParameters rocketParameters, Func<Vector3, Vector2> getGravitationForce, 
        Func<Vector3> getPlayerPosition, bool isPlayer, Action<PlanetController> onDeath)
    {
        this.isPlayer = isPlayer;
        this.radius = radius;
        this.angleSpeed = angleSpeed;
        this.angle = angle;
        spriteRenderer.color = isPlayer ? playerColor : color;
        this.rocketParameters = rocketParameters;
        launchCooldown = isPlayer ? rocketParameters.cooldown / 2.0f : rocketParameters.cooldown;
        launchTimer = launchCooldown;
        this.onDeath = onDeath;
        isInited = true;
        isActive = true;
        this.getGravitationForce = getGravitationForce;
        this.getPlayerPosition = getPlayerPosition;
        health = START_HEALTH;
        SetBarValue(heathBar, 1.0f);
        SetBarValue(cooldownBar, 0.0f);
    }

    public void DoDamage(float damage)
    {
        health -= damage;
        float barValue = Mathf.Clamp01(health / START_HEALTH);
        SetBarValue(heathBar, barValue);

        if (health <= 0.0f)
        {
            isActive = false;
            spriteRenderer.color = new Color32(50, 50, 50, 255);
            heathBarObject.SetActive(false);
            cooldownBarObject.SetActive(false);
            isDead = true;
            onDeath(this);
        }
    }

    public void SetPause(bool isPause)
    {
        isPaused = isPause;

        foreach (RocketController rocket in rockets)
        {
            rocket.SetPause(isPause);
        }
    }

    private void SetBarValue(Transform bar, float value)
    {
        bar.transform.localScale = new Vector3(value, bar.transform.localScale.y, bar.transform.localScale.z);
    }

    private void SetPosition()
    {
        transform.position = new Vector3(radius * Mathf.Cos((angle * Mathf.PI) / 180.0f), radius * Mathf.Sin((angle * Mathf.PI) / 180.0f), 0.0f);
    }

    private void Update()
    {
        if (!isInited || isPaused)
        {
            return;
        }

        angle += angleSpeed * Time.deltaTime;
        SetPosition();

        if (!isDead)
        {
            launchTimer -= Time.deltaTime;
            float barValue = 1.0f - Mathf.Clamp01(launchTimer / launchCooldown); 
            SetBarValue(cooldownBar, barValue);
        }

        if (isActive && !isDead && launchTimer < 0.0f)
        {
            if (isPlayer)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    LaunchRocket();
                    launchTimer = launchCooldown;
                }
            }
            else
            {
                launchTimer = launchCooldown;
                LaunchRocket();
            }
        }
    }

    private void LaunchRocket()
    {
        float startAngle;

        if (isPlayer)
        {
            Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            startAngle = GetAngle(mouseWorld);
        }
        else
        {
            Vector3 shootPoint = getPlayerPosition();
            startAngle = GetAngle(shootPoint) + UnityEngine.Random.Range(-30.0f, 30.0f);
        }

        planet.transform.eulerAngles = new Vector3(0.0f, 0.0f, startAngle);
        RocketController rocket = Instantiate(rocketPrefab, transform.parent);
        rocket.transform.position = rocketStartLocation.position;
        rocket.transform.eulerAngles = new Vector3(0.0f, 0.0f, startAngle);
        rocket.Init(rocketParameters, startAngle, getGravitationForce, OnRocketDestroy);
        rockets.Add(rocket);
    }

    private void OnRocketDestroy(RocketController rocket)
    {
        rockets.Remove(rocket);
    }

    private float GetAngle(Vector3 shootPoint)
    {
        Vector2 difference = new Vector2(shootPoint.x - transform.position.x, shootPoint.y - transform.position.y);
        float atan = 180.0f * Mathf.Atan(difference.y / difference.x) / Mathf.PI;
        float result;

        if (difference.x >= 0.0f && difference.y >= 0.0f)
        {
            result = atan;
        }
        else if (difference.x < 0.0f && difference.y >= 0.0f)
        {
            result = atan + 180.0f;
        }
        else if (difference.x >= 0.0f && difference.y < 0.0f)
        {
            result = atan;
        }
        else
        {
            result = atan - 180.0f;
        }

        return result;
    }
}
