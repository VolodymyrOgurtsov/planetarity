﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UISystem : MonoBehaviour
{
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private Text minPlanetsText;
    [SerializeField] private Text maxPlanetsText;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject resumeButton;
    [SerializeField] private GameObject winText;
    [SerializeField] private GameObject looseText;

    private Action onStartGame;
    private Action<bool> onIncreaseMinPlanets;
    private Action<bool> onIncreaseMaxPlanets;
    private Action<bool> onPause;

    public void Init(Action onStartGame, Action<bool> onIncreaseMinPlanets, Action<bool> onIncreaseMaxPlanets, Action<bool> onPause)
    {
        menuPanel.SetActive(true);
        gamePanel.SetActive(false);
        this.onStartGame = onStartGame;
        this.onIncreaseMinPlanets = onIncreaseMinPlanets;
        this.onIncreaseMaxPlanets = onIncreaseMaxPlanets;
        this.onPause = onPause;
    }

    public void OnStartGameButtonClick()
    {
        menuPanel.SetActive(false);
        gamePanel.SetActive(true);
        onStartGame();
    }

    public void UpdateMinPlanetsText(string text)
    {
        minPlanetsText.text = text;
    }

    public void UpdateMaxPlanetsText(string text)
    {
        maxPlanetsText.text = text;
    }

    public void ShowWinText()
    {
        winText.SetActive(true);
    }

    public void ShowLooseText()
    {
        looseText.SetActive(true);
    }

    public void OnIncreaseMinPlanetsButtonClick(bool isIncrease)
    {
        onIncreaseMinPlanets(isIncrease);
    }

    public void OnIncreaseMaxPlanetsButtonClick(bool isIncrease)
    {
        onIncreaseMaxPlanets(isIncrease);
    }

    public void OnPauseButtonClick(bool isPause)
    {
        pauseButton.SetActive(!isPause);
        resumeButton.SetActive(isPause);
        onPause(isPause);
    }
}
