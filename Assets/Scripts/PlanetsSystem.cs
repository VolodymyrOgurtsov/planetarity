﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetsSystem : MonoBehaviour
{
    [SerializeField] PlanetController planetPrefab;
    [SerializeField] GravitySystem gravitySystem;

    private float minRadius = 1.8f;
    private float maxRadius = 4.8f;
    private float maxAngleSpeed = 60.0f;
    private PlanetController player;
    private List<PlanetController> planets = new List<PlanetController>();
    private Action onWin;
    private Action onLoose;

    private Dictionary<RocketType, RocketParameters> rocketParameters = new Dictionary<RocketType, RocketParameters>
    {
        {RocketType.Red, new RocketParameters(Color.red, 6.0f, 0.5f, 5.0f, 6.0f)},
        {RocketType.Green, new RocketParameters(Color.green, 7.0f, 0.2f, 4.0f, 5.0f)},
        {RocketType.Blue, new RocketParameters(Color.blue, 8.0f, 0.1f, 3.0f, 4.0f)}
    };


    public void Init(int minPlanets, int maxPlanets, Action onWin, Action onLoose)
    {
        int planetsNumber = UnityEngine.Random.Range(minPlanets, maxPlanets + 1);
        float orbitStep = (maxRadius - minRadius) / (planetsNumber - 1);
        int playerNumber = UnityEngine.Random.Range(0, planetsNumber);
        this.onWin = onWin;
        this.onLoose = onLoose;

        for (int i = 0; i < planetsNumber; i++)
        {
            bool isPlayer = i == playerNumber;

            float radius = minRadius + i * orbitStep;
            float angleSpeed = maxAngleSpeed * Mathf.Pow(minRadius / radius, 1.5f);
            float alpha = UnityEngine.Random.Range(0.0f, 360.0f);
            Color32 color = new Color32((byte)UnityEngine.Random.Range(128, 256), (byte)UnityEngine.Random.Range(128, 256), (byte)UnityEngine.Random.Range(128, 256), 255);
            RocketType rocketType = (RocketType)UnityEngine.Random.Range(0, 3);
            PlanetController planet = Instantiate(planetPrefab, transform);
            planet.Init(radius, angleSpeed, alpha, color, rocketParameters[rocketType], gravitySystem.GetGravitationForce, GetPlayerPosition, isPlayer, OnDeath);
            planets.Add(planet);

            if (isPlayer)
            {
                player = planet;
            }

            gravitySystem.AddPlanet(planet);
        }
    }

    public void PauseGame(bool isPause)
    {
        foreach (PlanetController planet in planets)
        {
            planet.SetPause(isPause);
        }
    }

    private Vector3 GetPlayerPosition()
    {
        return player.transform.position;
    }

    private void OnDeath(PlanetController deadPlanet)
    {
        if (deadPlanet == player)
        {
            onLoose();
        }
        else
        {
            foreach (PlanetController planet in planets)
            {
                if (planet == player)
                {
                    continue;
                }

                if (!planet.IsDead)
                {
                    return;
                }
            }

            onWin();
        }
    }
}

public enum RocketType
{
    Red,
    Blue,
    Green
}

public struct RocketParameters
{
    public Color32 color;
    public float startSpeed;
    public float acceleration;
    public float damage;
    public float cooldown;

    public RocketParameters(Color32 color, float startSpeed, float acceleration, float damage, float cooldown)
    {
        this.color = color;
        this.startSpeed = startSpeed;
        this.acceleration = acceleration;
        this.damage = damage;
        this.cooldown = cooldown;
    }
}
