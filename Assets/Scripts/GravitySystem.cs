﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySystem : MonoBehaviour
{
    [SerializeField] Transform sunTransform;
    [SerializeField] private float sunMass = 20.0f;
    [SerializeField] private float planetMass = 5.0f;

    private List<PlanetController> planets = new List<PlanetController>();

    public void AddPlanet(PlanetController planet)
    {
        planets.Add(planet);
    }

    public Vector2 GetGravitationForce(Vector3 position)
    {
        Vector2 result = CalculateForce(sunMass, sunTransform, position);

        foreach (PlanetController planet in planets)
        {
            result += CalculateForce(planetMass, planet.transform, position);
        }

        return result;
    }

    private Vector2 CalculateForce(float mass, Transform body, Vector3 position)
    {
        float forceModule = mass * Mathf.Pow((body.position - position).magnitude,-2);
        return forceModule * (body.position - position).normalized;
    }
}
