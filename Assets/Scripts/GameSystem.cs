﻿using System;
using System.Collections;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
    [SerializeField] private PlanetsSystem planetsSystem;
    [SerializeField] private UISystem uiSystem;

    private int minMinPlanets = 2;
    private int minPlanets = 3;
    private int maxPlanets = 4;
    private int maxMaxPlanets = 5;

    private void Start()
    {
        uiSystem.Init(StartGame, OnIncreaseMinPlanets, OnIncreaseMaxPlanets, OnPause);
        uiSystem.UpdateMinPlanetsText(minPlanets.ToString());
        uiSystem.UpdateMaxPlanetsText(maxPlanets.ToString());
    }

    private void StartGame()
    {
        planetsSystem.Init(minPlanets, maxPlanets, OnWin, OnLoose);
    }


    private void OnIncreaseMinPlanets(bool isIncrease)
    {
        minPlanets = isIncrease ? minPlanets + 1 : minPlanets - 1;
        minPlanets = Mathf.Clamp(minPlanets, minMinPlanets, maxPlanets);
        uiSystem.UpdateMinPlanetsText(minPlanets.ToString());
    }

    private void OnIncreaseMaxPlanets(bool isIncrease)
    {
        maxPlanets = isIncrease ? maxPlanets + 1 : maxPlanets - 1;
        maxPlanets = Mathf.Clamp(maxPlanets, minPlanets, maxMaxPlanets);
        uiSystem.UpdateMaxPlanetsText(maxPlanets.ToString());
    }

    private void OnPause(bool isPause)
    {
        planetsSystem.PauseGame(isPause);
    }

    private void OnWin()
    {
        uiSystem.ShowWinText();
    }

    private void OnLoose()
    {
        uiSystem.ShowLooseText();
    }
}

