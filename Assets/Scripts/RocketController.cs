﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;

    private Vector2 acceleration;
    private float damage;
    private Func<Vector3, Vector2> getGravitationForce;
    private Vector2 speed;
    private bool isInited;
    private float destroyTime = 10.0f;
    private bool isPaused;
    private Action<RocketController> onRocketDestroy;

    public void Init(RocketParameters rocketParameters, float startAngle, Func<Vector3, Vector2> getGravitationForce, Action<RocketController> onRocketDestroy) 
    {
        spriteRenderer.color = rocketParameters.color;
        acceleration = new Vector2(rocketParameters.acceleration * Mathf.Cos((startAngle * Mathf.PI) / 180.0f), rocketParameters.acceleration * Mathf.Sin((startAngle * Mathf.PI) / 180.0f));
        damage = rocketParameters.damage;
        this.getGravitationForce = getGravitationForce;
        speed = new Vector2(rocketParameters.startSpeed * Mathf.Cos((startAngle * Mathf.PI) / 180.0f), rocketParameters.startSpeed * Mathf.Sin((startAngle * Mathf.PI) / 180.0f));
        isInited = true;
        this.onRocketDestroy = onRocketDestroy;
    }

    public void SetPause(bool isPause)
    {
        isPaused = isPause;
    }

    private void SetSpeedAndPosition()
    {
        transform.position += new Vector3(speed.x * Time.deltaTime, speed.y * Time.deltaTime, 0.0f);

        Vector2 totalForce = getGravitationForce(transform.position) + acceleration;
        speed += totalForce * Time.deltaTime;
    }

    private void Update()
    {
        if (!isInited || isPaused)
        {
            return;
        }

        SetSpeedAndPosition();

        destroyTime -= Time.deltaTime;

        if (destroyTime < 0.0f)
        {
            onRocketDestroy(this);
            Destroy(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Sun")
        {
            Destroy(gameObject);
        }
        else if (collision.tag == "Planet")
        {
            Destroy(gameObject);
            collision.GetComponent<PlanetController>().DoDamage(damage);
        }
    }
}
